#!/usr/bin/env python3

import requests
import json
    
from flask import Flask, request, render_template, send_from_directory
app = Flask(__name__)

@app.route('/')
def get_city():
    city = request.args.get('city')
    cities = []
    
    if city is not None and len(city.strip()) > 0:
        resp = requests.get("https://www.metaweather.com/api/location/search/?query=" + city)
        cities = json.loads(resp.text)
    else:
        city = None
    
    return render_template('/index.html', cities=cities, query=city)

@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('css', path)